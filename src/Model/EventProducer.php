<?php

declare(strict_types=1);

namespace Laraprooph\ModelEvent\Model;

use Laraprooph\ModelEvent\ModelChanged;

trait EventProducer
{
    /**
     * @var int
     */
    protected $version = 0;

    /**
     * @var array
     */
    protected $recordedEvents = [];

    public function popRecordedEvents(): array
    {
        $pendingEvents = $this->recordedEvents;

        $this->recordedEvents = [];

        return $pendingEvents;
    }

    protected function recordThat(ModelChanged $event): void
    {
        ++$this->version;

        $this->recordedEvents[] = $event->withVersion($this->version);

        $this->apply($event);
    }

    abstract protected function aggregateId(): string;

    abstract public function apply(ModelChanged $event): void;
}